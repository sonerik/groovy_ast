import groovy.json.JsonBuilder
import spock.lang.Specification

class JsonSerializableTest extends Specification {

    @JsonSerializable
    class SerializableTestObject {
        def strVal = "hi"
        def intVal = 1
        def floatVal = 2.5
        def innerSerializable = new InnerSerializableTestObject()

        @JsonSerializable
        class InnerSerializableTestObject {
            def innerStr = "HI"
            def innerInt = 100
            def innerFloat = 25.7
        }

    }

    def 'Annotated object should contain .toJson() method'() {
        setup:
        def testObject = new SerializableTestObject()

        expect:
        testObject.metaClass.methods.count { MetaMethod m ->
            m.name == 'toJson'
        } == 1
    }

    def "Annotated object's .toJson() should be equal to 'new JsonBuilder(testObject).toString()'"() {
        setup:
        def testObject = new SerializableTestObject()

        expect:
        testObject.toJson() == new JsonBuilder(testObject).toString()
    }

}
