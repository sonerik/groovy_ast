import groovyjarjarasm.asm.Opcodes
import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.builder.AstBuilder
import org.codehaus.groovy.ast.stmt.BlockStatement
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.ASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

@GroovyASTTransformation(phase=CompilePhase.SEMANTIC_ANALYSIS)
public class JsonSerializableASTTransformation implements ASTTransformation {

    @Override
    void visit(ASTNode[] nodes, SourceUnit source) {
        if (!checkNodes(nodes)) return

        ClassNode classNode = (ClassNode) nodes[1]
        classNode.addMethod(makeToJsonMethod())
    }

    static def checkNodes(ASTNode[] nodes) {
        nodes &&
                nodes[0] &&
                nodes[1] &&
                nodes[0] instanceof AnnotationNode &&
                nodes[0].classNode?.name == JsonSerializable.class.name &&
                nodes[1] instanceof ClassNode
    }

    static MethodNode makeToJsonMethod() {
        List<ASTNode> ast = new AstBuilder().buildFromString ('''
            new groovy.json.JsonBuilder(this).toString()
        ''')

        BlockStatement blockStatement = (BlockStatement) ast[0]
        new MethodNode('toJson', Opcodes.ACC_PUBLIC, new ClassNode(String.class), Parameter.EMPTY_ARRAY, ClassNode.EMPTY_ARRAY, blockStatement)
    }

}
